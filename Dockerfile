#Imagen base
FROM node:latest

#Directorio de la aplicacion
WORKDIR /app

#Copiado de archivos
ADD . /app

#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3000

#Comando para ejecutar la aplicacion
CMD ["npm", "start"]
